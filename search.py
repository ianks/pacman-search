# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and Pieter 
# Abbeel in Spring 2013.
# For more info, see http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html

"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""

import util
from IPython import embed

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other
    maze, the sequence of moves will be incorrect, so only use this for tinyMaze
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s,s,w,s,w,w,s,w]


class ActiveVertex:
    def __init__(self, current_state, action_sequence):
        self.current_state = current_state
        self.action_sequence = action_sequence



def depthFirstSearch(problem):
# Written Questions
#
# 1. Is the exploration order what you would have expected?
#   Yes, Although I do realize the order could have also been
#   reversed yet still had been a correct BFS. This was just how I
#   chose to code it.
#
# 2. Does Pacman actually go to all the explored squares on his way to the goal?
#   
#   All vertices are visited in a DFS.
#
# 3. Is this a least cost solution? If not, what is depth-first search doing wrong?
#   No it is not a least cost solution. In fact, it's not even guaranteed to be optimal.
#   However, it it GUARANTEED to find a solution without taking an infinite amount of time. 
#   It is not checking all of the successors and checking if the cost is minimized along the way.
#

    visited = util.Stack()
    queue = util.Stack()

    queue.push(ActiveVertex(problem.getStartState(),  []))

    while not queue.isEmpty():
        vertex = queue.pop()

        if problem.isGoalState(vertex.current_state):
            return vertex.action_sequence

        if vertex.current_state not in visited.list:
            visited.push(vertex.current_state)

            for state, action, cost in problem.getSuccessors(vertex.current_state):
                current_solution = vertex.action_sequence + [action]
                queue.push(ActiveVertex(state, current_solution))

    return []


class Solution:
    def __init__(self):
        self.visited = []
        self.action_path = []


def breadthFirstSearch(problem):
# Breadth First Search
#
# 1. Does BFS find a least cost solution?
#   No, but it finds an optimal path with the least amount of nodes.

    visited = util.Queue()
    queue = util.Queue()

    queue.push(ActiveVertex(problem.getStartState(),  []))

    while not queue.isEmpty():
        vertex = queue.pop()

        if problem.isGoalState(vertex.current_state):
            return vertex.action_sequence

        if vertex.current_state not in visited.list:
            visited.push(vertex.current_state)

            for state, action, cost in problem.getSuccessors(vertex.current_state):
                current_solution = vertex.action_sequence + [action]
                queue.push(ActiveVertex(state, current_solution))

    return []


def uniformCostSearch(problem):
    "Search the node of least total cost first. "
    "*** YOUR CODE HERE ***"

    visited = util.Queue()
    queue = util.PriorityQueue()

    queue.push(ActiveVertex(problem.getStartState(),  []), 0)

    while not queue.isEmpty():
        vertex = queue.pop()

        if problem.isGoalState(vertex.current_state):
            return vertex.action_sequence

        if vertex.current_state not in visited.list:
            visited.push(vertex.current_state)

            for state, action, cost in problem.getSuccessors(vertex.current_state):
                current_solution = vertex.action_sequence + [action]
                queue.push(ActiveVertex(state, current_solution), problem.getCostOfActions(current_solution))

    return []



def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    "*** YOUR CODE HERE ***"

    visited = util.Queue()
    queue = util.PriorityQueue()

    queue.push(ActiveVertex(problem.getStartState(),  []), 0)

    while not queue.isEmpty():
        vertex = queue.pop()

        if problem.isGoalState(vertex.current_state):
            return vertex.action_sequence

        if vertex.current_state not in visited.list:
            visited.push(vertex.current_state)

            for state, action, cost in problem.getSuccessors(vertex.current_state):
                current_solution = vertex.action_sequence + [action]
                queue.push(ActiveVertex(state, current_solution), problem.getCostOfActions(current_solution) + heuristic(state, problem))

    return []


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch

astar = aStarSearch
ucs = uniformCostSearch
