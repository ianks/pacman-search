Written Questions
=================

### Depth First Search

1. Is the exploration order what you would have expected?
  Yes, Although I do realize the order could have also been
  reversed yet still had been a correct BFS. This was just how I
  chose to code it.

2. Does Pacman actually go to all the explored squares on his way to the goal?
  
  All vertices are visited in a DFS.

3. Is this a least cost solution? If not, what is depth-first search doing wrong?
  No it is not a least cost solution. In fact, it's not even guaranteed to be optimal.
  However, it it GUARANTEED to find a solution without taking an infinite amount of time. 
  It is not checking all of the successors and checking if the cost is minimized along the way.


### Breadth First Search

1. Does BFS find a least cost solution?
  No, but it finds an optimal path with the least amount of nodes.
